#!/bin/bash

export color_prompt=yes

parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1) /'
}
export PS1="\n\[\033[32m\]\$(parse_git_branch)\[\033[00m\]\[\033[01;92m\]\u@\h\[\033[00m\]:\[\033[01;94m\]\w\[\033[00m\]\n\[\033[01;95m\]\$\[\033[00m\] "

export EDITOR=nvim

function init_ssh_agent() {
    ssh_sock=(`find /tmp -name '*agent*' -type s 2> /dev/null`)

    if [ -z ${ssh_sock[0]} ]; then
        echo "Did not find active ssh-agent. Starting up new."
        eval `ssh-agent -s`
        return
    fi

    sock_name=`echo ${ssh_sock[0]} | cut -d/ -f4`
    echo "Find active ssh-agent: " ${sock_name}
    export SSH_AUTH_SOCK=${ssh_sock[0]}

    unset ssh_sock sock_name
}

alias sagent="init_ssh_agent"
