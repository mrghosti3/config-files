#!/bin/bash

alias cd..='cd ..'
alias cls='clear'

alias c='wl-copy'
alias v='wl-paste'

alias kssh-agent='eval $(ssh-agent -k)'

alias rlldb="rust-lldb"
alias rgdb="rust-gdb"

alias ssh-work-proxy="ssh workbox_proxy"
