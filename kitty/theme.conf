#: Tab scheme

tab_bar_edge bottom
#: Which edge to show the tab bar on, top or bottom

# tab_bar_margin_width 0.0
#: The margin to the left and right of the tab bar (in pts)

tab_bar_style powerline
#: The tab bar style, can be one of: fade, separator, powerline, or
#: hidden. In the fade style, each tab's edges fade into the
#: background color, in the separator style, tabs are separated by a
#: configurable separator, and the powerline shows the tabs as a
#: continuous line.

# tab_bar_min_tabs 2
#: The minimum number of tabs that must exist before the tab bar is
#: shown

# tab_switch_strategy previous
#: The algorithm to use when switching to a tab when the current tab
#: is closed. The default of previous will switch to the last used
#: tab. A value of left will switch to the tab to the left of the
#: closed tab. A value of last will switch to the right-most tab.

# tab_fade 0.25 0.5 0.75 1
tab_fade 0 0 0 0

#: Control how each tab fades into the background when using fade for
#: the tab_bar_style. Each number is an alpha (between zero and one)
#: that controls how much the corresponding cell fades into the
#: background, with zero being no fade and one being full fade. You
#: can change the number of cells used by adding/removing entries to
#: this list.

# tab_separator " ┇"
#: The separator between tabs in the tab bar when using separator as
#: the tab_bar_style.

# tab_title_template {title}
#: A template to render the tab title. The default just renders the
#: title. If you wish to include the tab-index as well, use something
#: like: {index}: {title}. Useful if you have shortcuts mapped for
#: goto_tab N.

# active_tab_foreground   #000
active_tab_background   #8ae234
# active_tab_font_style   bold-italic
inactive_tab_foreground #eaeaea
inactive_tab_background #000000
# inactive_tab_font_style normal
#: Tab bar colors and styles


#: Color scheme

foreground #ffffff
background #100f0f

#: The foreground and background colors

background_opacity 0.8

#: The opacity of the background. A number between 0 and 1, where 1 is
#: opaque and 0 is fully transparent.  This will only work if
#: supported by the OS (for instance, when using a compositor under
#: X11). Note that it only sets the default background color's
#: opacity. This is so that things like the status bar in vim,
#: powerline prompts, etc. still look good.  But it means that if you
#: use a color theme with a background color in your editor, it will
#: not be rendered as transparent.  Instead you should change the
#: default background color in your kitty config and not use a
#: background color in the editor color scheme. Or use the escape
#: codes to set the terminals default colors in a shell script to
#: launch your editor.  Be aware that using a value less than 1.0 is a
#: (possibly significant) performance hit.  If you want to dynamically
#: change transparency of windows set dynamic_background_opacity to
#: yes (this is off by default as it has a performance cost)

# dynamic_background_opacity no

#: Allow changing of the background_opacity dynamically, using either
#: keyboard shortcuts (increase_background_opacity and
#: decrease_background_opacity) or the remote control facility.

# dim_opacity 0.75

#: How much to dim text that has the DIM/FAINT attribute set. One
#: means no dimming and zero means fully dimmed (i.e. invisible).

# selection_foreground #000000

#: The foreground for text selected with the mouse. A value of none
#: means to leave the color unchanged.

# selection_background #fffacd

#: The background for text selected with the mouse.


#: The 16 terminal colors. There are 8 basic colors, each color has a
#: dull and bright version. You can also set the remaining colors from
#: the 256 color table as color16 to color255.

color0 #242a32
color8 #666666

#: black

color1 #d60202
color9 #cb4b16

#: red

color2  #4e9a06
color10 #8ae234

#: green

color3  #c4a000
color11 #fce94f

#: yellow

color4  #3465a4
color12 #729fcf

#: blue

color5  #75507b
color13 #ad7fa8

#: magenta

color6  #06989a
color14 #34e2e2

#: cyan

color7  #d3d7cf
color15 #eeeeec

#: white
